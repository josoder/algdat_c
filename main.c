//
// Created by josoder on 05.09.17.
//

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "./structures/list.h"

void list_with_ints();

void int_iterator(void* data);

void string_iterator(void* data);

void free_string(void* data);

int main(int argc, char *argv[]){
    list_with_ints();
    list_with_strings();
}

void list_with_ints() {
    int numbers = 5;
    printf("generating list with %d positive numbers\n", numbers);

    list list;
    list_new(&list, sizeof(int), NULL);

    for (int i=0; i<=numbers; i++){
        list_add_head(&list, &i);
    }

    printf("size of the list is now %d \n", list_size(&list));
    int p;
    int h;
    int t;
    for(int i=0; i<3; i++) {
        list_head(&list, &p, true);
        list_head(&list, &h, false);
        printf("popped %d \n", p);
        printf("head is now: %d \n", h);
        list_tail(&list, &t);
        printf("tail is %d \n", t);
    }
    list_for_each(&list, int_iterator);
    printf("\n");

    list_destroy(&list);
}

void list_with_strings() {
    const char *verbs[] = {"shop", "lift", "drink", "eat", "sleep"};
    int size = 5;

    list list;
    // pass in the free function
    list_new(&list, sizeof(char *), free_string);

    char *verb;
    for(int i = 0; i<size; i++){
        verb = strdup(verbs[i]);
        list_add_tail(&list, &verb);
    }

    list_for_each(&list, string_iterator);
    list_destroy(&list);
}

void string_iterator(void *data) {
    printf("|%s", *(char **)data);
}

void int_iterator(void *data) {
    printf("|%d", *(int *)data);
}

void free_string(void *data)
{
    // typecast to the type stored in the list, so free(*(type in list)data)
    free(*(char **)data);
}