//
// Created by josoder on 08.09.17.
//

#ifndef DATASTRUCT_STACK_H
#define DATASTRUCT_STACK_H



#include "list.h"

typedef void (*freeFunction)(void *);

typedef struct {
    list *list;
} stack;

void stack_new_stack(stack *stack ,int elementSize, freeFunction *freeFunction);

void stack_destroy_stack(stack *stack);

void stack_push(stack *stack, void *element);

void stack_pop(stack *stack, void *element);

void stack_peek(stack *stack, void *element);

int stack_size(stack *stack);

#endif //DATASTRUCT_STACK_H
