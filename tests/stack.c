//
// Created by josoder on 08.09.17.
//
#include <string.h>
#include <assert.h>
#include <stdlib.h>

#include "../structures/stack.h"

void launch_tests(){

}

void test_init(){
    stack *stack;
    stack_new_stack(&stack, sizeof(int), NULL);
    assert(list_size(&stack) == 0);

    stack_destroy_stack(&stack);
}
